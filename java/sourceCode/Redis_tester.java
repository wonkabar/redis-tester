import redis.clients.jedis.Jedis;

public class Redis_tester {
	public static void main(String[] args) {
		Jedis j = new Jedis("docker.for.mac.localhost");
		// Jedis j = new Jedis("localhost");

		j.set("cs108", "awesome");
		System.out.println(j.get("cs108"));
//		System.out.println(j.get("cs108"));

		j.hset("tina", "name", "Archbold");
		j.hset("tina", "age", "10");
		j.hset("tina", "grade", "10th");

		System.out.println(j.hget("tina", "name"));
		System.out.println(j.hget("tina", "age"));
		System.out.println(j.hget("tina", "grade"));

	}

}
