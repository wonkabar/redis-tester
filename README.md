## Redis-Tester
 Project to show how to create a Redis Docker container and how to connect to it from either another container running Java or from a host machine running Java and using an IDE like Eclipse.

#### Running a stand alone Redis container
##### This can be done a couple of ways.
1. docker run 
    - run the container using a single 'docker run' command from your terminal. ```docker run -d -it --name my-redis -p 6666:6379 redis```
        - this command:
            - creates a docker container from the official redis image (redis)
            - tells it to run in the background (-d)
            - tells it to run interactively with a tty (-it)
            - names the container 'my-redis' (--name myredis)
            - maps port 6666 on the host to port 6379 in the container
                - by default redis accepts connections on port 6379
                - I am using port 6666 on the host because I tend to have other redis containers running on my host that use 6379 and no two Docker containers to map to the same port at the same time.
    - from here you can use your Host's Java installation to run code that connects to this redis container on **'localhost' port 6666**
        - Jedis library is required to connect to Redis using Java
            - [Jedis Library .jar](https://mvnrepository.com/artifact/redis.clients/jedis/2.4.2)
2. docker-compose
    - You could use the docker-compose file included in this project to run only the Redis container by running the following code from the root directory of this project.
        - ```docker-compose up -d redis```

#### Running a Redis container along side a Java container
coming soon



<iframe width="560" height="315" src="https://www.youtube.com/embed/wufeCaTNfuA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

